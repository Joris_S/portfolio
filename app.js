const express = require('express');
const expressEdge = require('express-edge');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const dotenv = require('dotenv').config();
const { check, validationResult  } = require('express-validator/check');


// Appels des controllers

const homeController = require('./controllers/home');

const app = new express();

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(expressEdge);
app.set("views", `${__dirname}/views`);

app.get("/", homeController);
app.post("/contact", (req,res) => {
    console.log(req.body);
});

app.post('/checkform',[
    check('nom').not().isEmpty().isLength({min : 5}).withMessage('Vous devez entrer votre prénom ainsi que votre nom.'),
    check('email').not().isEmpty().isEmail().withMessage('Veuillez insérer une adresse email valide.'),
    check('message').not().isEmpty().isLength({min : 30}).withMessage('Le message doit faire 30 caractères minimum.'),

],(req,res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    const message = `
        <p>Nouveau Message</p>
        <h3>Informations du contact</h3>
        <ul>
            <li>Nom : ${req.body.nom}</li>
            <li>Adresse : ${req.body.email}</li>
        </ul>
        <h3>Message</h3>
        <p>${req.body.message}</p>
    `
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.NODEMAILER_ADRESS,
            pass: process.env.NODEMAILER_PWD
        }
    });
    const mailOptions = {
        from: process.env.NODEMAILER_FROM, // sender address
        to: process.env.NODEMAILER_TO, // list of receivers
        subject: 'Message Portfolio', // Subject line
        html: message // plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if(err)
            console.log(err);
        else
            console.log(info);
    });

    res.render('index');
});

app.listen(80, () => {
    console.log('Application lancé sur le port 80');
});
